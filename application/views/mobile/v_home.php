<!DOCTYPE html>
<html >
	<head>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
		<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />

		<title>Home</title>
		<link href="<?php echo base_url().'assets/img/logo.png'?>" rel="shortcut icon" type="image/x-icon">

		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/bootstrap.min.css'?>" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/as.css'?>" />
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.min.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/bootstrap.min.js'?>"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	
	</head>
	<body>		
		<div class="container">	
			<?php
				$this->load->view('mobile/navbar');
			?>
			<div class="jumbotron">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-7">
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
						    <!-- Indicators -->
						    <ol class="carousel-indicators">
						      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						      <li data-target="#myCarousel" data-slide-to="1"></li>
						      <li data-target="#myCarousel" data-slide-to="2"></li>
						    </ol>

						    <!-- Wrapper for slides -->
						    <div class="carousel-inner">
						      <div class="item active">
						        <img src="<?php echo base_url().'assets/galeries/file_1542985830.jpg'?>" style="width:100%;">
						      </div>

						      <div class="item">
						        <img src="<?php echo base_url().'assets/galeries/file_1542985865.png'?>" style="width:100%;">
						      </div>
						    
						      <div class="item">
						        <img src="<?php echo base_url().'assets/galeries/file_1542985830.jpg'?>" style="width:100%;">
						      </div>

						    </div>

						    <!-- Left and right controls -->
						    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
						      <span class="glyphicon glyphicon-chevron-left"></span>
						      <span class="sr-only">Previous</span>
						    </a>
						    <a class="right carousel-control" href="#myCarousel" data-slide="next">
						      <span class="glyphicon glyphicon-chevron-right"></span>
						      <span class="sr-only">Next</span>
						    </a>
						  </div>													
						</div>
						<div class="col-md-5">
							<h3>About</h3>
							<p style=" font-size: 15px;">untuk membantu kegiatan sehari-hari, sehingga ketika mendesak dan malas pergi ke tempat printer tinggal pesan saja melalui Printline ini dan untuk hasilnya bisa diambil pada percetakan terdekat.</p>
						</div>
					</div>
				</div>

			</div>

		</div>		
	</body>
</html>