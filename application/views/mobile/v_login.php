<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
		<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />

		<title>Login</title>
		<link href="<?php echo base_url().'assets/img/logo.png'?>" rel="shortcut icon" type="image/x-icon">

		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/bootstrap.min.css'?>" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/as.css'?>" />
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.min.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/bootstrap.min.js'?>"></script>

		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	</head>
	<body >			
		<div class="container">
			<?php
				$this->load->view('mobile/navbar');
			?>
			<div class="jumbotron">
				<div class="panel panel-default">
					<div class="panel-heading" style="font-size: 14pt; background-color: #f4511e; color:white;">Form Login</div>
					<div class="panel-body">
						<?php echo $this->session->flashdata('msg');?>
						<form action="<?php echo base_url().'mobile/member/auth'?>" method="post" action="/action_page.php">
	
							<div class="form-group">
								<label for="email">Email</label>
								<input class="form-control" type="email" name="email" placeholder="Email*" required>
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input class="form-control" type="password" name="pass" placeholder="Password*" required>
							</div>
							<button type="submit" class="btn btn-default" style="background-color: #f4511e; color:white; border:0px; width:100%;">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>



			
			